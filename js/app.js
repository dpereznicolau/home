function showMenu() {
  var x = document.getElementById("topMenu");
  if (x.className === "topnav") {
    x.classList.add("responsive");
  } else {
    x.classList.remove("responsive");
  }
}

navAbout = document.getElementById("navAbout");
navAbout.addEventListener("click", function(){
  menuOpts = document.getElementsByClassName("menuOpt");
  for (var i = 0; i < menuOpts.length; i++) {
    menuOpts[i].classList.remove("active");
  }
  navAbout.classList.add("active");

  sections = document.getElementsByClassName("section");
  for (var i = 0; i < sections.length; i++) {
    if (sections[i].classList.contains("show")) {
      sections[i].classList.remove("show");
    }
    if (sections[i].id === "about") {
      sections[i].classList.add("show");
    }
  }
});

navProjects = document.getElementById("navProjects");
navProjects.addEventListener("click", function(){
  menuOpts = document.getElementsByClassName("menuOpt");
  for (var i = 0; i < menuOpts.length; i++) {
    menuOpts[i].classList.remove("active");
  }
  navProjects.classList.add("active");

  sections = document.getElementsByClassName("section");
  for (var i = 0; i < sections.length; i++) {
    if (sections[i].classList.contains("show")) {
      sections[i].classList.remove("show");
    }
    if (sections[i].id === "projects") {
      sections[i].classList.add("show");
    }
  }
});

navContact = document.getElementById("navContact");
navContact.addEventListener("click", function(){
  menuOpts = document.getElementsByClassName("menuOpt");
  for (var i = 0; i < menuOpts.length; i++) {
    menuOpts[i].classList.remove("active");
  }
  navContact.classList.add("active");

  sections = document.getElementsByClassName("section");
  for (var i = 0; i < sections.length; i++) {
    if (sections[i].classList.contains("show")) {
      sections[i].classList.remove("show");
    }
    if (sections[i].id === "contact") {
      sections[i].classList.add("show");
    }
  }
});